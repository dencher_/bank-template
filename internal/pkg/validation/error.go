package validation

import "errors"

var ErrViolation = errors.New("violation")

type Violation struct {
	Message string
	// Path string
	// Code string
}

func (v *Violation) Error() string {
	return v.Message
}

func (v *Violation) Is(target error) bool {
	return errors.Is(ErrViolation, target)
}
