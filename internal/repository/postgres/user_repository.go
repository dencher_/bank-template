package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"bank/internal/domain"
	"bank/internal/pkg/persistence"

	"github.com/gofrs/uuid"
)

type UserRepository struct {
	connection persistence.Connection
}

func NewUserRepository(connection persistence.Connection) *UserRepository {
	return &UserRepository{
		connection: connection,
	}
}

func (r *UserRepository) FindByIDForUpdate(ctx context.Context, id uuid.UUID) (*domain.User, error) {
	row := r.connection.QueryRow(ctx, "SELECT id, name FROM auth.user WHERE id = $1 FOR UPDATE", id)

}

func (r *UserRepository) Update(ctx context.Context, user *domain.User) error {
	// TODO implement me
	panic("implement me")
}

func (r *UserRepository) Save(ctx context.Context, user *domain.User) error {
	query, args, err := psql.
		Insert(`auth.app_user`).
		Columns("id", "name", "password").
		Values(user.ID(), user.Name(), user.PasswordHash()).
		ToSql()
	if err != nil {
		return fmt.Errorf("build insert user query: %w", err)
	}

	_, err = r.connection.Exec(ctx, query, args...)
	if err != nil {
		return fmt.Errorf("insert user: %w", err)
	}

	return nil
}

func (r *UserRepository) FindByName(ctx context.Context, name string) (*domain.User, error) {
	// TODO: sql query
	row := r.connection.QueryRow(ctx, "query")

	var user struct {
		ID   uuid.UUID
		Name string
	}
	err := row.Scan(&user.ID)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, persistence.ErrEntityNotFound
	}
	if err != nil {
		return nil, err
	}

	return domain.NewUser(user.ID, user.Name), nil
}
