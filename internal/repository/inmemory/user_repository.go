package inmemory

import (
	"context"
	"sync"

	"bank/internal/domain"
)

type UserRepository struct {
	users map[string]*domain.User
	mx    sync.Mutex
}

func NewUserRepository() *UserRepository {
	return &UserRepository{users: make(map[string]*domain.User)}
}

func (r *UserRepository) Save(_ context.Context, user *domain.User) error {
	r.mx.Lock()
	defer r.mx.Unlock()

	r.users[user.Name()] = user

	return nil
}

func (r *UserRepository) FindByName(username string) (*domain.User, error) {
	r.mx.Lock()
	defer r.mx.Unlock()

	user, ok := r.users[username]
	if !ok {
		return nil, domain.ErrNotFound
	}

	return user, nil
}
