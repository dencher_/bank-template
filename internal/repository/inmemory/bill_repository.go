package inmemory

import (
	"context"

	"bank/internal/domain"

	"github.com/gofrs/uuid"
)

type BillRepository struct {
	bills map[uuid.UUID]*domain.Bill
}

func NewBillRepository() *BillRepository {
	return &BillRepository{
		bills: make(map[uuid.UUID]*domain.Bill),
	}
}

func (repository *BillRepository) Save(ctx context.Context, bill *domain.Bill) error {
	repository.bills[bill.ID()] = bill

	return nil
}
