package domain

import (
	"context"

	"github.com/gofrs/uuid"
)

type User struct {
	id   uuid.UUID
	name string
	// password is HASH of user password
	passwordHash string
}

func (u *User) ID() uuid.UUID        { return u.id }
func (u *User) Name() string         { return u.name }
func (u *User) PasswordHash() string { return u.passwordHash }

func NewUser(name string, hash []byte) *User {
	return &User{
		id:           uuid.Must(uuid.NewV7()),
		name:         name,
		passwordHash: string(hash),
	}
}

func (u *User) Update(name string) {
	u.name = name
}

type UserSearchParameters struct {
	Name         string
	Page         int
	ItemsPerPage int

	ID uuid.UUID
}

type UserRepository interface {
	Save(ctx context.Context, user *User) error
	FindByName(ctx context.Context, name string) (*User, error)
	// FindByParameters(ctx context.Context, parameters UserSearchParameters) ([]*User, error)
}
