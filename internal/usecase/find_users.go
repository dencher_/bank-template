package usecase

import (
	"context"

	"bank/internal/domain"
)

type FindUsersUseCase struct {
	users domain.UserRepository
}

type FindUsersQuery struct {
	Name string
}

func (u *FindUsersUseCase) Handle(ctx context.Context, query FindUsersQuery) ([]*domain.User, error) {
	parameters := domain.UserSearchParameters{
		Name: query.Name,
	}

	return u.users.FindByParameters(ctx, parameters)
}
