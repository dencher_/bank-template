package handlers

import (
	"encoding/json"
	"errors"
	"net/http"

	"bank/internal/pkg/validation"
	"bank/internal/usecase"
)

type GETUsersHandler struct {
	useCase *usecase.FindUsersUseCase
}

// GET /api/users?name=Ва
// Returns [{"id": "b6deb361-014a-4a7a-99ef-82ffb77ae99a", "name": "Вася"}]
func (handler *GETUsersHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	query := request.URL.Query()

	name := query.Get("name")

	users, err := handler.useCase.Handle(request.Context(), usecase.FindUsersQuery{Name: name})
	if errors.Is(err, validation.ErrViolation) {
		http.Error(writer, err.Error(), http.StatusUnprocessableEntity)

		return
	}
	if err != nil {
		// TODO
	}

	if err := json.NewEncoder(writer).Encode(users); err != nil {
		// TODO
	}
}
