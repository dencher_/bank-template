package di

import (
	"context"
	"net/http"
	"os"

	"bank/internal/domain"
	"bank/internal/handlers"
	"bank/internal/handlers/middleware"
	"bank/internal/pkg/persistence"
	postgrespkg "bank/internal/pkg/persistence/postgres"
	"bank/internal/repository/postgres"
	"bank/internal/usecase"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Container struct {
	err error

	secretKey   string
	databaseURL string

	connection persistence.Connection

	router http.Handler

	createBills *usecase.CreateBillUseCase
	createUser  *usecase.CreateUserUseCase

	billsRepository domain.BillRepository
	userRepository  domain.UserRepository

	postBillsHandler    *handlers.POSTBillsHandler
	postRegisterHandler *handlers.POSTRegisterHandler
}

func (c *Container) DatabaseURL() string {
	if c.databaseURL == "" {
		c.databaseURL = os.Getenv("DATABASE_URL")
	}

	return c.databaseURL
}

func (c *Container) Err() error {
	return c.err
}

func (c *Container) Pool(ctx context.Context) persistence.Connection {
	if c.connection == nil {
		postgresPool, err := pgxpool.New(ctx, c.DatabaseURL())
		if err != nil {
			panic(err)
		}

		if err := postgresPool.Ping(ctx); err != nil {
			panic(err)
		}

		c.connection = postgrespkg.NewPoolConnection(postgresPool)
	}

	return c.connection
}

func (c *Container) SecretKey() string {
	if c.secretKey == "" {
		c.secretKey = os.Getenv("JWT_SECRET_KEY")
	}

	return c.secretKey
}

func (c *Container) UserRepository(ctx context.Context) domain.UserRepository {
	if c.userRepository == nil {
		return postgres.NewUserRepository(c.Pool(ctx))
	}

	return c.userRepository
}

func (c *Container) SetUserRepository(userRepository domain.UserRepository) {
	c.userRepository = userRepository
}

func NewContainer() *Container {
	return &Container{}
}

func (c *Container) PostBillsHandler(ctx context.Context) *handlers.POSTBillsHandler {
	if c.postBillsHandler == nil {
		c.postBillsHandler = handlers.NewPOSTBillsHandler(c.CreateBills(ctx))
	}

	return c.postBillsHandler
}

func (c *Container) CreateBills(ctx context.Context) *usecase.CreateBillUseCase {
	if c.createBills == nil {
		c.createBills = usecase.NewCreateBillUseCase(c.BillsRepository(ctx))
	}

	return c.createBills
}

func (c *Container) BillsRepository(ctx context.Context) domain.BillRepository {
	if c.billsRepository == nil {
		c.billsRepository = postgres.NewBillRepository(c.Pool(ctx))
	}

	return c.billsRepository
}

func (c *Container) SetBillsRepository(billsRepository domain.BillRepository) {
	c.billsRepository = billsRepository
}

func (c *Container) HTTPRouter(ctx context.Context) http.Handler {
	if c.router != nil {
		return c.router
	}

	router := mux.NewRouter()
	router.Use(
		middleware.PanicMiddleware,
		// middleware.AuthMiddleware,
	)

	router.Handle("/bills", c.PostBillsHandler(ctx)).Methods(http.MethodPost)
	router.Handle("/register", c.PostRegisterHandler(ctx)).Methods(http.MethodPost)

	c.router = router

	return c.router
}

func (c *Container) PostRegisterHandler(ctx context.Context) http.Handler {
	if c.postRegisterHandler == nil {
		c.postRegisterHandler = handlers.NewPOSTRegisterHandler(c.CreateUser(ctx))
	}

	return c.postRegisterHandler
}

func (c *Container) CreateUser(ctx context.Context) *usecase.CreateUserUseCase {
	if c.createUser == nil {
		c.createUser = usecase.NewCreateUserUseCase(c.UserRepository(ctx), c.SecretKey())
	}

	return c.createUser
}

func (c *Container) Close(ctx context.Context) {
	if c.connection != nil {
		c.connection.Close()
	}
}
