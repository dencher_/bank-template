-- +goose Up
-- +goose StatementBegin
ALTER TABLE auth.app_user ADD COLUMN password text NOT NULL DEFAULT '';
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE auth.app_user DROP COLUMN password;
-- +goose StatementEnd
