-- +goose Up
-- +goose StatementBegin
CREATE SCHEMA auth;

CREATE TABLE auth.app_user (
    id uuid PRIMARY KEY,
    name text NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE auth.app_user;

DROP SCHEMA auth;
-- +goose StatementEnd







