-- +goose Up
-- +goose StatementBegin
CREATE SCHEMA bank;

CREATE TABLE bank.bill (
    id uuid PRIMARY KEY,
    name text NOT NULL,
    balance int NOT NULL DEFAULT 0
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE bank.bill;

DROP SCHEMA bank;
-- +goose StatementEnd
