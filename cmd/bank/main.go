package main

import (
	"context"
	"flag"
	"log"
	"net/http"

	"bank/internal/di"
	"bank/internal/repository/inmemory"
)

func main() {
	isTest := flag.Bool("test", false, "")

	flag.Parse()

	ctx := context.Background()

	container := di.NewContainer()
	defer container.Close(ctx)

	if *isTest {
		container.SetBillsRepository(inmemory.NewBillRepository())
		container.SetUserRepository(inmemory.NewUserRepository())
	}

	err := http.ListenAndServe(":8080", container.HTTPRouter(ctx))
	if err != nil {
		log.Fatal(err)
	}
}
